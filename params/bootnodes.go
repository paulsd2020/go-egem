// Copyright 2015 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package params

// MainnetBootnodes are the enode URLs of the P2P bootstrap nodes running on
// the main Ethereum network.
var MainnetBootnodes = []string{
	// Egem Go Bootnodes
	"enode://248a543351cfd0c47470f3bfa8b8ec583760e439d75874285053abe0f43e9c93b09984cad70d3b786052cf9c641070b2553489c96509453228cd400649374c2f@167.114.153.196:30666",
	"enode://92c19dcc5d5cbd1eb1277c4206864b9238c1597371611dafce911102f893f3235d70520c776cc997b0aac803b3075494ad7c03a35654f73f17f11de9df82cbd5@192.99.167.242:30666",
	"enode://9dcdbf3357b6e637ed1722b3b1f62815ba33ec2567b773a2b8cf4ef9ea063023fba0f37428644e31494f9176370a2fdcdf67f940a90d84149d4664b8c8ae46e9@51.68.174.86:30666",
	"enode://2c5f28085e0cd07f923dc787bc7cedf3b2a7978b606eaf1ec82fd0ac0cb46729c09663edc35bfa56f68eab11a082eb6b58e1a9f8d3372bf8dfa08430ace140fa@107.152.32.6:30666",
	"enode://f2074e180bc90204f43335052c64d7066796acb3859b55adf9aee27c2f7cebbd4ffa9daab9e9a4300097226e7a9dd7e7aaabac5d486c9ac9731bb8bfbfda329e@203.28.238.124:30666",
	"enode://9690c6d0ec8515fa7ac780ce5a617b1ed8b84923e4bcc397cdd7595dea193e9b170b81512e834b3133ab56489dbeb29e4d8228efac7519dc372d7886eb0558fa@49.247.139.30:30666",
	"enode://ab874b9eeb8c4a5064b88c3a399e3d19b6cf3470f45a22ad517bdf9efb8b266180e99bba578f44f759f3ecb059064318e4eaec09f207281a3ce53bbbd9a3728c@157.245.37.195:30666",
	"enode://ea962400cefde523349b88eebb48269c8b7ae617aa58afbd3baa3139325e7c55a54a27f8bed4b62c524b6b92247a9402b599e7061c948e7b414d2fa3307746ff@138.197.152.148:30666?discport=1221",
	"enode://b3f8b0819f305fdfd396d4ce9d55807c607a594b6b42669c2f07ba08a8fc364464de9220133746854b343c7c661b23a11f53840bf9743baa12ab74a28c108617@206.189.79.94:30666",
	"enode://4bcfc03676af78cf4f2da393a7df7ea23b312ab0e2da0ac045312e24c07e5c4d0da8c332251addbbca5e075eef3063705299222bd39d837495e618d29178b121@54.38.215.141:53136",
	"enode://bf59fa8ad3463d403479b5316a3c3180db5bf0aaf3ebab6a85275bced0f10c8a38f2874a0f0e7c36065f8596fe20605454048c9ccb7fc0f5be1c9e6a81774711@76.8.60.153:30666",
	"enode://905c0ef77f6701bf8f2150acb198d561574b0b4147b59dbaba00c158220a655806bfe25133b19a870deb64a0ebd72fd79a67049d1fc84bb0eb082bbf77435285@94.212.157.20:30666",
	"enode://08ed875fde050ef3d57028899db5612c12e73edefe30f8319c8657e3b5c73e63928816b5c949507611e1dd59fc17978f1d8422033e6b124abcf7e6d024e1778c@167.179.75.204:30666",
	"enode://c5c4bd1ad9e18d2bda55522cca67ca1c3df50f39841ef87cbe64b94b9302298e93c184810b30ec496c4e35cfe7c812891c1a5f8c334046d9476be5e39c4dd54b@144.91.84.70:30666",
	"enode://27ae1579bdccc2933e86b87c3d5b62e4f374848dfd71e4dfcf83e5337b32e9e5cca4286f51750e4703364ab424918a06ccef0878277a24610e7ec1462d42da92@172.105.35.243:30666",
	"enode://8e99326b6f84b9e06e8f186d181ae1a948aae0ea1c28faa78773d6c982995fff864f05f63eb5dd77bee10a8baf9fff4b7f14ed75ecef5823ed98c7c669bc6e82@116.202.112.233:30666",
	"enode://006037a9109bbb45c17595254cccd9242ee6cdcf00ffa6e397e9ce905fbc2ef437bc84e5697ad8bfdb66ddf40133e11d7167bab7df2ecd45174fff5e922ac67a@95.179.232.170:30666",
	"enode://82d51904eda722bace7e8b289720d686ecab9142d24c2926bceeb4745e8228833e3b580f4c83cb20ab6250880780178aa8dc94b7143a56246019406d24e66fd2@203.28.238.135:30666",
	"enode://ec8a427444a75221b040e4e76a644596acfb468ac9451c9b720290808a9a654f95a29418635994f9c638f8f45e27fa0276513db199c90809af0fc8ffb8f6ee91@95.179.179.238:30666",
	"enode://bb8832633d72d5042fa819ea7c8084b2b49bad8f74b02b184ec204453902d959d84378fef56e6240a728b2f6328750ec177ec1f2fb9cae003b85b88a0866d952@96.30.195.204:30666",
	"enode://d091c6173cf790bebdf5bb0365c1f7755c5cf4a7a7abeb6cbb8b01b354ec9c26bad435a8c3441820a4d7a42f82d0eef914e2d1b3736fdd78bfe67b842558316c@45.76.94.222:30666",
	"enode://e43cca85df2011ec9743e9652cc4ef68f39d9e7dbca80b77b9947fddcd04fd10ae04ebcf781df606d4928f3e587000c17a7708aec055096ddd359beb7fc9580d@93.189.43.70:30666?discport=33678",
	"enode://b8412785565874980057d0aa3664fd8565d71b27d244c6d24bfd24866157a6eaca38b65f453a4b05deda01b5847b5ecaa27a32cd1fc56b58a16d524a78a24cbd@178.128.206.73:30666",
	"enode://59fa041bde42b26a2b6118220e5eee9f8df0b6c372e417a956fdd1a30129e76ae533b61b90dd9ad827522c2de8e5d840be0f6a86ce32bcc52c04a95a102a5ec0@157.245.46.208:30666",
	"enode://41a4e474e41aaa2aee6bdfbfcd7623c5f7f95d7ffafa07bb8c604d58a9481ab8b4382579a640a95a4d6d0434fb1cdc9b92953178fa4008413b860768e8b50246@45.76.78.124:30666",
	"enode://081ae4c5a6f860f86043520c53d557b5af5e92db42cdfbfd083dba0a5a0ae9f899e337d0473ae839fdd0128770217008b7e478795f36cd77c99ce2903bbfe34f@90.145.247.160:30666",
	"enode://04b9b040f38ea05d84a2d7b33c32acff2a2023ec3667c0da7df583b977cae8cbac8d57a0e4f15c1049c3ae7f5f7632e4c823e4a3efbd159787d8b03d74789f3e@173.255.247.213:30666",
}

// TestnetBootnodes are the enode URLs of the P2P bootstrap nodes running on the
// Ropsten test network.
var TestnetBootnodes = []string{}

// RinkebyBootnodes are the enode URLs of the P2P bootstrap nodes running on the
// Rinkeby test network.
var RinkebyBootnodes = []string{}

// GoerliBootnodes are the enode URLs of the P2P bootstrap nodes running on the
// Görli test network.
var GoerliBootnodes = []string{}

// DiscoveryV5Bootnodes are the enode URLs of the P2P bootstrap nodes for the
// experimental RLPx v5 topic-discovery network.
var DiscoveryV5Bootnodes = []string{}
